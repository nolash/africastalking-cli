from setuptools import setup

f = open('./README.md', 'r')
long_description = f.read()
f.close()

setup(
        name='africastalking-cli',
        version='0.0.1',
        description='CLI interface to africas talking python library',
        author='Louis Holbrook',
        author_email='dev@holbrook.no',
        license='GPL3',
        #long_description=long_description,
        #long_description_content_type='text/markdown',
        install_requires=[
            'confini~=0.2.7',
            'phonenumbers~=8.12.12',
            'africastalking==1.2.3',
            'pyxdg~=0.27',
        ],
        packages=[
            'africastalking_cli',
        ],
        scripts = [
            'scripts/at-cli.py',
            ],
        )
